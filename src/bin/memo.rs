#![feature(test)]

extern crate test;

struct JustOnStack {
    a: u8,
    #[cfg(feature = "with-unused-struct-member")]
    b: [u8; 1024],
}

#[inline(never)]
fn recurse(depth: usize) {
    if depth != 0 {
        let a = JustOnStack {
            a: 1,
            #[cfg(feature = "with-unused-struct-member")]
            b: [0; 1024]
        };
        test::black_box(&a.a);
        recurse(depth - 1);
        test::black_box(&a.a);
    }
}

fn main() {
    recurse(1024 * 10);
}
