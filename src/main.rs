#![feature(test)]

extern crate test;

static TESTCRI: &[u8] = b"123";

struct Cri {
    data: &'static [u8],
}

fn iterate(s: &[u8]) -> u8 {
    let mut x: u8 = 0;
    for (i, c) in s.iter().enumerate() {
        x = x.wrapping_add(*c * (i as u8));
    }
    for (i, c) in s.iter().enumerate() {
        x = x.wrapping_add(*c * (i as u8));
    }
    for (i, c) in s.iter().enumerate() {
        x = x.wrapping_add(*c * (i as u8));
    }
    x
}

impl Cri {
    fn part1_end(&self) -> usize {
        if iterate(self.data) == 0 {
            // Not the case here but the compiler can't know that easily
            99
        } else {
            1
        }
    }

    fn part1(&self) -> &'static [u8] {
        &self.data[..self.part1_end()]
    }

    fn part2(&self) -> &'static [u8] {
        &self.data[self.part1_end()..]
    }
}

#[bench]
fn simplest_duplicate(b: &mut test::Bencher) {
    b.iter(|| {
        let c = Cri { data: test::black_box(TESTCRI) };
        // If any fn common subexpression can be evaluated, it should be this.
        let a = c.part1_end();
        let b = c.part1_end();
        test::black_box(a + b);
    })
}

#[bench]
fn full_example(b: &mut test::Bencher) {
    b.iter(main)
}

#[bench]
fn single_calculation(b: &mut test::Bencher) {
    b.iter(|| {
        let c = Cri { data: test::black_box(TESTCRI) };
        test::black_box(c.part1_end());
    })
}

fn main() {
    let c = Cri { data: test::black_box(TESTCRI) };
    let p1 = c.part1();
    test::black_box(p1);
    let p2 = c.part2();
    test::black_box(p2);
}
