Can the compiler optimize what could otherwise be done in type state?
=====================================================================

In developing a binary variation of URIs (CRIs_) for use with microcontrollers,
trade-offs need to be made between representation size and parsing complexity.

Being read in a linear fashion (like any URI -- scheme, host, port, path, query etc.),
CRIs sure *can* be parsed without wasting stack space:
Read the scheme, remember the cursor position, process the scheme, read the next part et cetera.
But should we *have to* hold the compiler's hand in doing so?

There are four ways I came up with in which a CRI can be processed;
the focus points are efficiency
(don't use stack space or cycles, as we may need to process lots of them at the same time)
and API usability
(make it easy for the user to use, and to use efficiently):

1. Full type stating.

   The initial CRI type can do only one operation giving the first component (scheme) and a tail object,
   which in turn can only give component 2 (host and port) and a tail object,
   etc.,
   with the final tail object indicating wellformedness.

   .. code:: rust

       impl CRI<'a> for MyCRI {
           type Tail: impl CRITail1;

           fn pull(self) -> (&'a str, Self::Tail);
       }

   This forces the user to access everything in sequence,
   and forces the compiler's hand in keeping only the necessary information,
   but is a complex API in comparison (especially as in actual CRIs, some components are actually iterators).

2. Manual non-type state machine.

   Each time a step in the typical pulling-out sequence is done,
   the internal state is mutated through an enum that represents the reading position and carries the currently relevant context information.

   Out-of-sequence reads can either panic, or advance/reset the state machine.

   .. code:: rust

       enum State {
           Initial,
           ReadScheme { hostindex: usize },
           // ...
       }

       struct MyCRI<'a> {
           slice: &'a [u8],
           state: Cell<State>;
       }

       impl CRI<'a> for MyCRI {
           fn scheme(&self) -> &'a str {
               if let Initial = self.scheme.get() {
                   // parse something
                   self.state.set(ReadScheme { hostindex: cursor });
                   scheme
               } else {
                   panic!()
               }
           }
       }

   This is sub-optimal in terms of development effort (needs state machine as compared to the later versions),
   and can either panic (hoping the compiler recognizes that this does not happen in code that uses it right)
   and needs to carry additional information around ("where are we", which could be known statically),
   but may represent a good spot if the below both fail.

   (By using a core::cell::Cell, the interior mutation could be hidden
   without incurring actual synchronization cost
   and only losing the Sync property,
   which should be OK).

3. Memoization.

   The CRI struct has members for seek information for the components,
   populating them as used (or in advanced),
   and the provides access to all parts.

   .. code:: rust

       struct MyCRI<'a> {
           slice: &'a [u8],
           hostindex: Cell<Option<usize>>,
           // ...
       }

       impl MyCRI<'a> {
           fn hostindex() -> usize {
               self.hostindex.get().unwrap_or_else(|| {
                   // calculate hostindex
                   self.hostindex.set(Some(hostindex))
                   hostindex
               })
           }
       }

       impl CRI<'a> for MyCRI<'a> {
           fn scheme(&self) -> &'a str {
               self.data[..self.hostindex()]
           }
       }

   This fills up several pointers on the stack (or uses up registers).
   The compiler could be smart and see that the struct is actually never used as a whole,
   and that it actually only ever needs one of them at one time when used in a program where the components are accessed sequentially.

   (To my knowledge, there is no requirement for a struct to be ever realized in stack memory as a whole,
   as long as no pointers to it are passed out).

4. Duplicate calculation.

   When later members are accessed, the CRI is just iterated over again in full.
   This is wasteful in terms of computation cycles,
   but maybe the compiler could see that results used here are still as they were before,
   and keep them around
   in an advanced form of Common Subexpression Evaluation.
   (It may depend on the calculation to be a const fn to be eligible for CSE,
   but that can be provided).

   Then, when used in sequence, no calculation would actually be done twice.

   .. code:: rust

       struct MyCRI<'a> {
           slice: &'a [u8],
       }

       impl MyCRI<'a> {
           fn hostindex() -> usize {
               // calculate hostindex
               hostindex
           }

           fn pathindex() -> usize {
               let hostindex = self.hostindex();
               // calculate pathindex from there
               pathindex
           }
       }

This repository is for exploring whether the optimizations in 3. and 4. happen in practice, so that we could Just Let The Compiler Do It.

.. _CRIs: https://github.com/core-wg/coral/

Usage
-----

To test duplicate calculation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Run ``cargo bench``.

If the calculation deduplication were to work, ``simplest_duplicate`` and ``full_example`` should take about as much time as ``single_calculation``.

While the simplest duplicate test does take roughy twice as long as the single calculation,
the full example *does* run faster than would be expected if the calculation were done twice.

This is, however, only true for the "s" optimization level
(which is the common level in embedded development);
in others (including 3 and "z"), both the full example and simple duplicate take twice the time of the single calculation,
as was expected for when the optimization is not performed.

Preliminary exploration at godbolt_ seems to confirm that
the part1_end function is only evaluated once (look at the production of the number 99, it is only traversed once).

Further exploration is indicated as to what influences whether the optimization is performed,
whether it can be influenced by further annotation,
and whether it can be preserved to actual CRI parsing.

.. _godbolt: https://godbolt.org/#g:!((g:!((g:!((h:codeEditor,i:(fontScale:14,fontUsePx:'0',j:1,lang:rust,selection:(endColumn:23,endLineNumber:12,positionColumn:23,positionLineNumber:12,selectionStartColumn:23,selectionStartLineNumber:12,startColumn:23,startLineNumber:12),source:'%23!!%5Bfeature(test)%5D%0A%0Aextern+crate+test%3B%0A%0Astatic+TESTCRI:+%26%5Bu8%5D+%3D+b%22123%22%3B%0A%0Astruct+Cri+%7B%0A++++data:+%26!'static+%5Bu8%5D,%0A%7D%0A%0Afn+iterate(s:+%26%5Bu8%5D)+-%3E+u8+%7B%0A++++let+mut+x:+u8+%3D+0%3B%0A++++for+(i,+c)+in+s.iter().enumerate()+%7B%0A++++++++x+%3D+x.wrapping_add(*c+*+(i+as+u8))%3B%0A++++%7D%0A++++x%0A%7D%0A%0Aimpl+Cri+%7B%0A++++fn+part1_end(%26self)+-%3E+usize+%7B%0A++++++++if+iterate(self.data)+%3D%3D+0+%7B%0A++++++++++++//+Not+the+case+here+but+the+compiler+can!'t+know+that+easily%0A++++++++++++99%0A++++++++%7D+else+%7B%0A++++++++++++1%0A++++++++%7D%0A++++%7D%0A%0A++++fn+part1(%26self)+-%3E+%26!'static+%5Bu8%5D+%7B%0A++++++++%26self.data%5B..self.part1_end()%5D%0A++++%7D%0A%0A++++fn+part2(%26self)+-%3E+%26!'static+%5Bu8%5D+%7B%0A++++++++%26self.data%5Bself.part1_end()..%5D%0A++++%7D%0A%7D%0A%0Apub+fn+main()+%7B%0A++++let+c+%3D+Cri+%7B+data:+test::black_box(TESTCRI)+%7D%3B%0A++++let+p1+%3D+c.part1()%3B%0A++++test::black_box(p1)%3B%0A++++let+p2+%3D+c.part2()%3B%0A++++test::black_box(p2)%3B%0A%7D%0A'),l:'5',n:'0',o:'Rust+source+%231',t:'0')),k:28.95804242303345,l:'4',n:'0',o:'',s:0,t:'0'),(g:!((h:compiler,i:(compiler:nightly,filters:(b:'0',binary:'1',commentOnly:'0',demangle:'0',directives:'0',execute:'1',intel:'0',libraryCode:'0',trim:'1'),fontScale:14,fontUsePx:'0',j:1,lang:rust,libs:!(),options:'-C+opt-level%3Ds+-C+panic%3Dabort',selection:(endColumn:1,endLineNumber:1,positionColumn:1,positionLineNumber:1,selectionStartColumn:1,selectionStartLineNumber:1,startColumn:1,startLineNumber:1),source:1),l:'5',n:'0',o:'rustc+nightly+(Editor+%231,+Compiler+%231)+Rust',t:'0')),k:37.708624243633224,l:'4',n:'0',o:'',s:0,t:'0'),(g:!((h:cfg,i:(editorid:1,j:1,options:(navigation:'1',physics:'1'),pos:(___x:54.88477892986911,___y:-420.168729002038),scale:0.6337115356773528,selectedFn:'example::main:'),l:'5',n:'0',o:'rustc+nightly+Graph+Viewer+(Editor+%231,+Compiler+%231)',t:'0')),k:33.33333333333333,l:'4',n:'0',o:'',s:0,t:'0')),l:'2',n:'0',o:'',t:'0')),version:4

To test elision of unused struct components
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Run ``cargo run --bin memo --release``.

Observe that it doesn't overflow the stack.

Add with ``--features with-unused-struct-member``.

Observe that it *does* now overflow the stack,
even though the struct member added is never used in this application.

(This is an even easier version than the desired;
for the full optimization the compiler would need to see which members are used in which phase even).

This holds across different opt levels, and also when panic=abort is active.

Results
-------

Duplicate calculation can get optimized away,
under circumstances that need further characterization.

For manual memoization, no optimizations could be observed:
Even in a vastly simplified example,
the full struct is kept around.


Pointers to any methodological errors
or pending compiler features that may allow one or the other optimization would be much appreciated,
eg. to <mailto:chrysn@fsfe.org>.
